class FestivelnetController < ApplicationController
    def index1
        @title = "Lists of event"
        @festivelnets = Festivelnet.list(params)
        @data = Array.new
        @festivelnets.each do |x|
            @data << {name: x.eventname, attendance: x.attendance, start_date: x.date, show_director: x.show_director, show_director_email_id: x.show_director_email_id}
         end
    end
    
    def index
        @response = Festivelnet.all
        @data1 = Array.new
        @response.each do |x|
            @data1 << {name: x.eventname, attendance: x.attendance, start_date: x.date, show_director: x.show_director, show_director_email_id: x.show_director_email_id}
        end
    end
end
