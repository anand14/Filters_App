class Festivelnet < ApplicationRecord
    def self.list(params)
        arel = Festivelnet.all
        if params[:select1] == 'AND' 
            arel = arel.where("eventname LIKE ?", ("%" + params[:search] + "%")) if params[:search].present?
            arel = arel.where("attendance = ?", params[:attendee]) if params[:attendee].present?	
        else
            arel = arel.where("eventname LIKE ? OR attendance = ?", ("%" + params[:search] + "%"), params[:attendee]) if params[:search].present? && params[:attendee].present?
            arel = arel.where("eventname LIKE ?", ("%" + params[:search] + "%")) if params[:search].present? && !params[:attendee].present?
            arel = arel.where("attendance = ?",  params[:attendee]) if !params[:search].present? && params[:attendee].present?
        end
        if params[:select2] == 'AND' 
            arel = arel.where("date BETWEEN ? AND ?", params[:from_date][0], params[:to_date][0]) if params[:from_date][0].present? && params[:to_date][0].present?
            arel = arel.where("date >= ?", params[:from_date]) if params[:from_date].present? && !params[:to_date].present?
            arel = arel.where("date <= ?", params[:to_date]) if params[:to_date].present? && !params[:from_date].present?
        else
            result = Array.new
            result = Festivelnet.where("date BETWEEN ? AND ?", params[:from_date][0], params[:to_date][0]) if params[:from_date][0].present? && params[:to_date][0].present?
            result = Festivelnet.where("date >= ?", params[:from_date]) if params[:from_date].present? && !params[:to_date].present?
            result = Festivelnet.where("date <= ?", params[:to_date]) if params[:to_date].present? && !params[:from_date].present?
            arel = (arel + result)
        end
        arel	
    end
end
