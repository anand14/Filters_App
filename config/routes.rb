Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/' => 'festivelnet#index'
  post '/festivelnet_options' => 'festivelnet#index1'

end
