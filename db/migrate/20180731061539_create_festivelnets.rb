class CreateFestivelnets < ActiveRecord::Migration[5.2]
  def change
    create_table :festivelnets do |t|
      t.string :eventname
      t.timestamp :date
      t.string :address
      t.string :event_address
      t.integer :attendance
      t.string :showpromoter
      t.string :showpromoter_href
      t.string :show_promoter_email_ID
      t.string :show_promoter_website
      t.string :show_director
      t.string :show_director_email_id
      t.string :show_director_phone_number
      t.string :food_director
      t.string :food_director_email_ID
      t.string :food_director_phone_number
      t.string :foodbooth
      t.string :foodboothfees
      t.string :foodtruck
      
      t.timestamps
    end
  end
end
