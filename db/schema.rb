# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_07_31_061539) do

  create_table "festivelnets", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "eventname"
    t.timestamp "date"
    t.string "address"
    t.string "event_address"
    t.integer "attendance"
    t.string "showpromoter"
    t.string "showpromoter_href"
    t.string "show_promoter_email_ID"
    t.string "show_promoter_website"
    t.string "show_director"
    t.string "show_director_email_id"
    t.string "show_director_phone_number"
    t.string "food_director"
    t.string "food_director_email_ID"
    t.string "food_director_phone_number"
    t.string "foodbooth"
    t.string "foodboothfees"
    t.string "foodtruck"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
