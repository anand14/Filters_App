require 'csv'

namespace :extract do
    desc 'Extract data from csv file and store the data in the table'

    task :csv_recurring => :environment do 
        csv_data = File.read("#{Rails.root}/sample.csv")
        data = CSV.parse(csv_data, :headers => true)
        data.each do |row|
            # puts row.to_hash
            Festivelnet.create!(row.to_hash)
        end
    end

    task :csv_once => :environment do 
        csv_data = File.read("#{Rails.root}/sample.csv")
        data = CSV.parse(csv_data, :headers => true)
        record_array = Array.new
        data.each do |row|
            record_array << row.to_hash
        end
        # print record_array
        Festivelnet.create!(record_array)
    end
end